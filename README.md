# EfficientIP SOLIDserver Provider
This provider allows to easily interact with EfficientIP's [SOLIDserver](https://www.efficientip.com/products/solidserver/) REST API.
It allows to manage supported resources through CRUD operations for efficient DDI orchestration.

<p align="center">
  <a align="center" href="https://www.efficientip.com/resources/video-what-is-ddi/">
    <img width="560" src="https://i.ytimg.com/vi/daQ0xEWNvYY/maxresdefault.jpg" title="What is DDI ?">
  </a>
</p>

This source and binary repository is now available and maintained [here](https://github.com/EfficientIP-Labs/terraform-provider-solidserver)
